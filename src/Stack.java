public class Stack {


    private String Tabarr[];
    private int rear;
    private int capacite;      // Pour connaitre la capacité du tableau
    private int size;         // Pour savoir la taille en temps reel du tableau

    private int size() {
        return size;
    }

    public Boolean isEmpty() {
        return (size() == 0);
    }

    public Boolean isFull() {
        return (size() == capacite);
    }

    Stack(int size) {
        Tabarr = new String[size];
        capacite = size;
        rear = -1;

    }

    public void push(String Value) {

        if (isFull()) {
            System.out.println("La liste est full");
        } else {

            System.out.println("Objet inseré  :" + Value);
        }
        rear = rear + 1;
        Tabarr[rear] = Value;
        size++;
    }

    public void pop() {

        if (isEmpty()) {
            System.out.println("La liste est vide");
        } else {
            System.out.println("Objet supprimé :" + Tabarr[rear]);
                for ( int i =0 ; i <Tabarr.length; i++){
                    Tabarr[rear]= null;
                }

        }

        rear = (rear - 1);
        size--;
    }

    public String peek() {
        if (isEmpty()) {
            System.out.println("La Queue est vide");
        }
        return Tabarr[rear];
    }




    public void  Afficher() {

        for (int i = 0; i < size; i++)
        {
           System.out.println(Tabarr[i]);
        }



    }
}
